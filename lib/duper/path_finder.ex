defmodule Duper.PathFinder do
  use GenServer

  @me PathFinder

  @spec start_link(any) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(root) do
    GenServer.start_link(__MODULE__, root, name: @me)
  end

  @spec next_path :: any
  def next_path() do
    GenServer.call(@me, :next_path)
  end

  @spec init(binary | maybe_improper_list) :: :ignore | {:error, any} | {:ok, pid}
  def init(path) do
    DirWalker.start_link(path)
  end

  def handle_call(:next_path, _from, dir_walker) do
    path =
      case DirWalker.next(dir_walker) do
        [path] -> path
        foo -> foo
      end

    {:reply, path, dir_walker}
  end
end
